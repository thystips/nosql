= Compte rendu

_Premiere je m'excuse pour la suite parce que j'ai un probleme de clavier et je ne peux pas faire d'accents pour le moment, mes touches mortes ne reagissent plus (QWERTY US international) suite a une MAJ._

== Remarques sur Redis

J'avais deja utilise redis pour du cache avec des services comme harbor ou netbox, sur kubernetes j'ai eu pas mal de problemes pour la mise en cluster de la solution d'ou ma connaissance de sentinel. +
Apres je n'ai pas d'avis sur redis, c'est visiblement une solution qui fait ce qu'on lui demande quand on sait l'utiliser mais qui a vocation a etre utilisee principalement par des devs, ce que je ne suis pas. +
Par contre je ne suis pas fan de ce genre de tutoriel pour apprendre, ca donne une idee mais personnellement je sais que je ne retiendrais pas grand chose de ce que j'y ai fait.

== Avancement du tuto

J'ai fini le tutoriel.

== Choses non comprises

Le tutoriel n'etait tres clair sur certains points mais en completant avec le doc c'est bon

== Questions

Pas de question particuliere.

== Dans mon domaine

Pour du stockage cle/valeur quelconque (cache,queue), je le vois plus comme une dependence d'un service que je dois mettre en place que quelque chose que je compte consommer directement meme si je ne doute pas que pour les dev il y a plus d'applications.

_Encore desole pour cet histoire d'accents, pour ecrire du francais c'est vraiment illisible._